# Ruby on Rails Tutorial: "hello, world!"

![Screenshot](screenshot.png)

This is the first application for the
[*Ruby on Rails Tutorial*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

You can visit this app at <https://hellorails-app.herokuapp.com/>

## Installation

Make sure you have postgresql installed:

```bash
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib
```

Make sure you have the `libpq` library in your system

```bash
sudo apt install libpq-dev
```

Install all gems

```bash
bundle install
```

## Running the app

```bash
bundle exec rails s
```

Then go to <http://localhost:3000/>



## Installing the Heroku cli


You can install the heroku cli as a gem, but it is better to [install it in your system](https://devcenter.heroku.com/articles/heroku-cli) so that it is available to you for any language and not only in Ruby.

```bash
sudo add-apt-repository "deb https://cli-assets.heroku.com/branches/stable/apt ./"
curl -L https://cli-assets.heroku.com/apt/release.key | sudo apt-key add -
sudo apt update
sudo apt install heroku
heroku --version
```


## Pushing to Heroku

Pushing to heroku will deploy the application.

Make sure you have an account in Heroku and log in:

```bash
heroku login
> Enter your Heroku credentials:
> Email: user@example.com
> Password: ***********
> Logged in as user@example.com
```

Create the app if it doesn't exist yet

```bash
heroku create
```

Make sure that the app is in your remotes

```bash
heroku git:remote -a yourapp
```

Then push

```bash
git push heroku master
```

